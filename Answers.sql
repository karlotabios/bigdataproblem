--1. Which discussion tags involved the most engagement?

map = function() {
	var tags = this.tags;
	var people_engaged = this.views;

	for (var i = 0; i < tags.length; i++) {
		emit(
			{
				tags: tags[i],
			}, {
				times_appeared: 1,
				people_engaged: people_engaged,
			}
		);
	}
}

reduce = function(key, values) {
	var total = { times_appeared: 0, people_engaged: 0 };
	for (var k = 0; k < values.length; k++) {
		total.times_appeared += values[k].times_appeared;
		total.people_engaged += values[k].people_engaged;
	}
	return total;
}

results = db.runCommand({
	mapReduce: 'tedtalks',
	map: map,
	reduce: reduce,
	out: 'tedtalks.engagement'
});

db.tedtalks.engagement.find().pretty();

--2. What was the reception from the audience towards the general event based from the talks in terms of positivity or negativity?

map = function() {
	var talk = this.title;
	var event = this.event;
	var ratings = this.ratings;
	var talk_positivity = 0;
	var talk_negativity = 0;
	var talk_disposition = 0;

	for (var i = 0; i < ratings.length; i++) {
		if (( ratings[i].name == "Unconvincing" ) || ( ratings[i].name == "Obnoxious" ) || ( ratings[i].name == "Longwinded" ) || ( ratings[i].name == "Confusing" )) {
				talk_negativity += ratings[i].count;
			} else {
				talk_positivity += ratings[i].count;
			}
		}

	talk_disposition = talk_positivity - talk_negativity;

	emit(
		{
			event: event,
			talk: talk
		}, 
		{
			talk_positivity: talk_positivity,
			talk_negativity: talk_negativity,
			talk_disposition: talk_disposition
		}
	);
}

reduce = function(key, values) {
}

results = db.runCommand({
	mapReduce: 'tedtalks',
	map: map,
	reduce: reduce,
	out: 'tedtalks.talk_ratings'
});

db.tedtalks.talk_ratings.find( {}, {} ).pretty();

map = function() {
	var event = this._id.event;
	var event_positivity = this.value.talk_positivity;
	var event_negativity = this.value.talk_negativity;
	var event_disposition = this.value.talk_disposition;
	var event_positivty_negativity_index = event_positivity / event_negativity;

	emit(
		{
			event: event,
		}, 
		{
			event_positivity: event_positivity,
			event_negativity: event_negativity,
			event_disposition: event_disposition,
			event_positivty_negativity_index: Math.round(event_positivty_negativity_index)
		}
	);
}

reduce = function(key, values) {
	var event_positivity = 0;
	var event_negativity = 0;
	var event_disposition = 0;

	for (var k = 0; k < values.length; k++) {
		event_positivity += values[k].event_positivity;
		event_negativity += values[k].event_negativity;
		event_disposition += values[k].event_disposition;
	}

	var event_positivty_negativity_index = event_positivity / event_negativity;

	var foo = {
		event_positivity: event_positivity,
		event_negativity: event_negativity,
		event_disposition: event_disposition,
		event_positivty_negativity_index: Math.round(event_positivty_negativity_index)
	}

	return foo;
}

results = db.runCommand({
	mapReduce: 'tedtalks.talk_ratings',
	map: map,
	reduce: reduce,
	out: 'tedtalks.event_ratings'
});

db.tedtalks.event_ratings.find( {}, {} ).pretty();