NOTE: Not included in this git are the volumes for docker mongodb sharding and replicates. Including those will add gigabytes to this git. If you want them, you have to download them yourself by running the docker-compose command in the respective replication/sharding folder (Set them up yourself).

A. How to load the dataset
1. Assuming your setup of the docker service is ready in the replication folder, go into the terminal and copy the file inside the data folder via “docker cp”

docker cp ted_main.csv bigdataproblem_mongosetup_1:/ted_main.csv

2. enter into the container of mongosetup

docker-compose run mongosetup sh

3. execute the command "mongoimport" to load the dataset into the primary mongo node

mongoimport --db tedtalks --collection tedtalks --type csv --headerline --file ted_main.csv --host mongo1 --port 27017

4. go into the database interface via the command "mongo" while at the bash

mongo —-host mongo1:27017 tedtalks

5. enter the command "use tedtalks"

6. run the following command in order to cleanse the data (this data has stringyfied JSON, which we cannot use and must parse through JSON.parse())
NOTE: these same commands can be found in the Data Cleanser.js file

db.tedtalks.find( { tags : { $type : 2 } } ).snapshot().forEach( function (x) {
		var tags_string = x.tags;
		tags_string = tags_string.replace(/\'/g, '"'); tags_string = tags_string.replace(/\"/g, "'"); tags_string = tags_string.replace(/Alzheimer\'s/gi, 'Alzheimers'); tags_string = tags_string.replace(/\'/g, '"'); var foo = JSON.parse(tags_string); x.tags = foo; db.tedtalks.save(x); });


db.tedtalks.find( { ratings : { $type : 2 } } ).snapshot().forEach( function (x) {
		var ratings_string = x.ratings;
		ratings_string = ratings_string.replace(/\'/g, '"'); var ratings_object = JSON.parse(ratings_string); x.ratings = ratings_object; db.tedtalks.save(x); });

B. How to setup the Replicate sets

1. Go into the replication folder

2. Create a "docker-compose.yml" file in your project folder if it’s not yet there (it should be there).

// 3. If you’re creating the docker.yml file, copy the following to the newly created "docker-compose.yml"

version: '3'
services:
  mongo3:
    image: mongo:3.2
    hostname: mongo3
    expose:
      - "27017"
    volumes:
      - ./mongo3:/data/db
    restart: always
    entrypoint: [ "/usr/bin/mongod", "--replSet", "tedtalks", "--rest", "--httpinterface" ]
  mongo2:
    image: mongo:3.2
    hostname: mongo2
    expose:
      - "27017"
    volumes:
      - ./mongo2:/data/db
    restart: always
    entrypoint: [ "/usr/bin/mongod", "--replSet", "tedtalks", "--rest", "--httpinterface" ]
  mongo1:
    image: mongo:3.2
    hostname: mongo1
    expose:
      - "27017"
    volumes:
      - ./mongo1:/data/db
    restart: always
    entrypoint: [ "/usr/bin/mongod", "--replSet", "tedtalks", "--rest", "--httpinterface" ]
  mongosetup:
    image: mongo:3.2
    expose:
      - "27017"
    restart: always

4. Start your services
docker-compose up

5. Login to "mongosetup"
docker-compose run mongosetup sh

6. Login to "mongo1"
mongo --host mongo1:27017 tedtalks

7. Setup the configuration for the replica set with the following javascript

var cfg = {
 "_id" : "tedtalks",
 "version" : 1,
 "members" : [
  {
     "_id" : 0,
     "host" : "mongo1:27017",
     "priority": 1
  },
  {
    "_id": 1,
    "host": "mongo2:27017",
    "priority": 0
  },
  {
    "_id": 2,
    "host": "mongo3:27017",
    "priority": 0
  }
 ]
}

8. Initiate the replica set using the configuration
rs.initiate( cfg );

9. After initiating, set and find the reading preference to the nearest node

db.getMongo().setReadPref('nearest');

10. Exit from the current node
exit

11. Login to the secondary node i.e. 'mongo2'
mongo --host mongo2:27017 tedtalks

12. Everytime you switch a node, execute this command.
db.setSlaveOk()

C. How to execute the MapReduce functions
NOTE: Raw code for the MapReduce functions are found in the Answer.sql file

1. for the first question of the big data problem, run the following code

map = function() {
	var tags = this.tags;
	var people_engaged = this.views;

	for (var i = 0; i < tags.length; i++) {
		emit(
			{
				tags: tags[i],
			}, {
				times_appeared: 1,
				people_engaged: people_engaged,
			}
		);
	}
}

reduce = function(key, values) {
	var total = { times_appeared: 0, people_engaged: 0 };
	for (var k = 0; k < values.length; k++) {
		total.times_appeared += values[k].times_appeared;
		total.people_engaged += values[k].people_engaged;
	}
	return total;
}

results = db.runCommand({
	mapReduce: 'tedtalks',
	map: map,
	reduce: reduce,
	out: 'tedtalks.engagement'
});

db.tedtalks.engagement.find().pretty();


2. for the second question of the big data problem, run the following code


map = function() {
	var talk = this.title;
	var event = this.event;
	var ratings = this.ratings;
	var talk_positivity = 0;
	var talk_negativity = 0;
	var talk_disposition = 0;

	for (var i = 0; i < ratings.length; i++) {
		if (( ratings[i].name == "Unconvincing" ) || ( ratings[i].name == "Obnoxious" ) || ( ratings[i].name == "Longwinded" ) || ( ratings[i].name == "Confusing" )) {
				talk_negativity += ratings[i].count;
			} else {
				talk_positivity += ratings[i].count;
			}
		}

	talk_disposition = talk_positivity - talk_negativity;

	emit(
		{
			event: event,
			talk: talk
		}, 
		{
			talk_positivity: talk_positivity,
			talk_negativity: talk_negativity,
			talk_disposition: talk_disposition
		}
	);
}

reduce = function(key, values) {
}

results = db.runCommand({
	mapReduce: 'tedtalks',
	map: map,
	reduce: reduce,
	out: 'tedtalks.talk_ratings'
});

db.tedtalks.talk_ratings.find( {}, {} ).pretty();

map = function() {
	var event = this._id.event;
	var event_positivity = this.value.talk_positivity;
	var event_negativity = this.value.talk_negativity;
	var event_disposition = this.value.talk_disposition;
	var event_positivty_negativity_index = event_positivity / event_negativity;

	emit(
		{
			event: event,
		}, 
		{
			event_positivity: event_positivity,
			event_negativity: event_negativity,
			event_disposition: event_disposition,
			event_positivty_negativity_index: Math.round(event_positivty_negativity_index)
		}
	);
}

reduce = function(key, values) {
	var event_positivity = 0;
	var event_negativity = 0;
	var event_disposition = 0;

	for (var k = 0; k < values.length; k++) {
		event_positivity += values[k].event_positivity;
		event_negativity += values[k].event_negativity;
		event_disposition += values[k].event_disposition;
	}

	var event_positivty_negativity_index = event_positivity / event_negativity;

	var foo = {
		event_positivity: event_positivity,
		event_negativity: event_negativity,
		event_disposition: event_disposition,
		event_positivty_negativity_index: Math.round(event_positivty_negativity_index)
	}

	return foo;
}

results = db.runCommand({
	mapReduce: 'tedtalks.talk_ratings',
	map: map,
	reduce: reduce,
	out: 'tedtalks.event_ratings'
});

db.tedtalks.event_ratings.find( {}, {} ).pretty();

D. How to run the MapReduce functions
NOTE: Section E of this README have discussed this already

1. Assuming everything from E. have been accomplished, simple run the following commands in sequence

results = db.runCommand({
	mapReduce: 'tedtalks',
	map: map,
	reduce: reduce,
	out: 'tedtalks.engagement'
});

results = db.runCommand({
	mapReduce: 'tedtalks',
	map: map,
	reduce: reduce,
	out: 'tedtalks.talk_ratings'
});

results = db.runCommand({
	mapReduce: 'tedtalks.talk_ratings',
	map: map,
	reduce: reduce,
	out: 'tedtalks.event_ratings'
});

E. How to shard the MapReduce collection

1. Go into the sharing folder.

2. Create a "docker-compose.yml" file in your project folder if it’s not there yet (it should be).

3. Start your services
docker-compose up

4. Login to "mongosetup"
docker-compose run mongosetup sh

5. Login to "mongos"
mongo mongos1:27017/tedtalks

6. Use and login to tedtalks (database)
use tedtalks

7. To add nodes to the sharding set
db.adminCommand( { addshard : "node1:27017" } )
db.adminCommand( { addshard : "node2:27017" } )

8. To enable sharding for a database
db.adminCommand( { enablesharding : "tedtalks" } )

9. Exit out of the mongo node
exit

10. import the data into the mongo node from the outside
mongoimport --db tedtalks --collection tedtalks --type csv --headerline --file ted_main.csv --host mongos1 --port 27017

11. Go back into the node
mongo mongos1:27017/tedtalks


12. Create the index needed to shard
  db.tedtalks.createIndex(
    { 'event': 1 },
    { name: 'event' }
  )

13. Start the sharding
  sh.shardCollection( 
    "tedtalks.event", 
    { "tedtalks.event": 1 }
  )